package pl.edu.agh.globulos

import pl.edu.agh.globulos.game.Game.PitchConfig
import pl.edu.agh.globulos.game.GameManager
import pl.edu.agh.globulos.ml._

import scala.collection.mutable
import scala.language.postfixOps

object Tournament3v3 {
  val pitchConfig: PitchConfig = PitchConfig(
    pitchWidth = 20, pitchHeight = 13, maxForce = 1200
  )

  val teams: Seq[(String, () => Player)] = Seq(
    ("Noop", () => new NoopPlayer),
    ("Random", () => new RandomPlayer),
    ("AtBall", () => new AtBallPlayer),
    ("SmartAtBall", () => new SmartAtBallPlayer),
    ("NN (Noop)", () => NN3v3VectorPlayer.load(0, "nn3v3_noop_best.model")),
    ("NN (Random)", () => NN3v3VectorPlayer.load(0, "nn3v3_rand_best.model")),
    ("NN (AtBall)", () => NN3v3VectorPlayer.load(0, "nn3v3_ab_best.model")),
    ("NN (SmartAtBall)", () => NN3v3VectorPlayer.load(0, "nn3v3_sab_best.model")),
    ("BNN (40)", () => NN3v3VectorPlayer.load(0, "big_nn_40_best.model")),
    ("BNN (60.1)", () => NN3v3VectorPlayer.load(0, "big_nn_60_best1.model")),
    ("BNN (60.2)", () => NN3v3VectorPlayer.load(0, "big_nn_60_best2.model")),
    ("BNN (60.3)", () => NN3v3VectorPlayer.load(0, "big_nn_60_best3.model")),
    ("BNN (60.D)", () => NN3v3VectorPlayer.load(0, "big_nn_60_best_defence.model")),
    ("BNN (100.1)", () => NN3v3VectorPlayer.load(0, "big_nn_100_best1.model")),
    ("BNN (100.2)", () => NN3v3VectorPlayer.load(0, "big_nn_100_best2.model")),
    ("BNN (100.3)", () => NN3v3VectorPlayer.load(0, "big_nn_100_best3.model")),
    ("BNN (100.4)", () => NN3v3VectorPlayer.load(0, "big_nn_100_best4.model")),
    ("BNN (200.1)", () => NN3v3VectorPlayer.load(0, "big_nn_200_best1.model")),
    ("BNN (200.2)", () => NN3v3VectorPlayer.load(0, "big_nn_200_best2.model")),
    ("BNN (200.3)", () => NN3v3VectorPlayer.load(0, "big_nn_200_best3.model")),
    ("BNN (200.4)", () => NN3v3VectorPlayer.load(0, "big_nn_200_best4.model")),
    ("BNN (200.5)", () => NN3v3VectorPlayer.load(0, "big_nn_200_best5.model")),
    ("BNN (200.6)", () => NN3v3VectorPlayer.load(0, "big_nn_200_best6.model")),
    ("BNN (200.7)", () => NN3v3VectorPlayer.load(0, "big_nn_200_best7.model")),
    ("BNN (400.1)", () => NN3v3VectorPlayer.load(0, "big_nn_400_best1.model")),
    ("BNN (400.2)", () => NN3v3VectorPlayer.load(0, "big_nn_400_best2.model"))
  )

  def main(args: Array[String]): Unit = {
    val points = mutable.Map.empty[String, Int].withDefaultValue(0)
    val wins = mutable.Map.empty[String, Int].withDefaultValue(0)
    val loses = mutable.Map.empty[String, Int].withDefaultValue(0)

    for (i <- teams.indices) {
      for (j <- i + 1 until teams.size) {
        val p1 = teams(i)
        val p2 = teams(j)
        val t1 = p1._2()
        val t2 = p2._2()

        println(s"${p1._1} vs ${p2._1}")

        var (first, draws, second) = (0, 0, 0)
        t1.trainingMode(false)
        t2.trainingMode(false)
        (1 to 1).foreach { _ =>
          val gameMgr = new GameManager(t1, t2)
          gameMgr.play(3, 3, 5, realTime = false, randomStart = false, pitchConfig = pitchConfig) match {
            case Some(1) =>
              first += 1
              wins(p1._1) += 1
              loses(p2._1) += 1
            case Some(2) =>
              second += 1
              loses(p1._1) += 1
              wins(p2._1) += 1
            case _ =>
              draws += 1
          }
        }

        points(p1._1) += first * 3 + draws
        points(p2._1) += second * 3 + draws

        println(s"Result: $first:$draws:$second")
      }
    }

    println("Final results: ")
    println(
      points.toSeq.sortBy(-_._2).map { r =>
        s"${r._1} - ${r._2} (${wins(r._1)}/${loses(r._1)})"
      }.mkString("\n")
    )
  }
}
