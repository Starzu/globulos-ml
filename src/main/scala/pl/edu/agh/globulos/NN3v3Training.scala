package pl.edu.agh.globulos

import pl.edu.agh.globulos.game.Game.PitchConfig
import pl.edu.agh.globulos.game.GameManager
import pl.edu.agh.globulos.ml._

import scala.language.postfixOps
import scala.util.Try

object NN3v3Training {
  val pitchConfig: PitchConfig = PitchConfig(
    pitchWidth = 20, pitchHeight = 13, maxForce = 1200
  )

  def main(args: Array[String]): Unit = {
    val iterations = args(0).toInt
    val batch = args(1).toInt
    val testIterations = args(2).toInt
    val filenamePrefix = args(3)
    val filenamePrefix2 = args(4)
    val initExplore = Try(args(5).toDouble).getOrElse(0.0)
    val initExplore2 = Try(args(6).toDouble).getOrElse(0.0)
    val loadFromFile = if (args.length >= 8) Some(args(7)) else None
    val loadFromFile2 = if (args.length >= 9) Some(args(8)) else None

    val playerOne = loadFromFile match {
      case Some(filename) => NN3v3VectorPlayer.load(initExplore, filename)
      case None => NN3v3VectorPlayer(initExplore)
    }
    val playerTwo = new NoopPlayer
//    val playerTwo = loadFromFile2 match {
//      case Some(filename) => NN3v3VectorPlayer.load(initExplore2, filename)
//      case None => NN3v3VectorPlayer(initExplore2)
//    }

    for (i <- 1 to (iterations / batch)) {
      println(s"Batch: $i")
      playerOne.trainingMode(true)
      playerTwo.trainingMode(true)
      training(batch, playerOne, playerTwo)
      playerOne.save(s"${filenamePrefix}_${i * batch}.model")
      playerTwo.save(s"${filenamePrefix2}_${i * batch}.model")
      playerOne.trainingMode(false)
      playerTwo.trainingMode(false)
      test(testIterations, playerOne, playerTwo)
    }
  }

  def training(iterations: Int, playerOne: Player, playerTwo: Player): Unit =
    (1 to iterations).map { _ =>
      val gameMgr = new GameManager(playerOne, playerTwo)
      gameMgr.play(3, 3, 10, realTime = false, randomStart = false, pitchConfig = pitchConfig)
    }

  def test(iterations: Int, playerOne: Player, playerTwo: Player): Unit = {
    var (first, draws, second) = (0, 0, 0)
    (1 to iterations).foreach { _ =>
      val gameMgr = new GameManager(playerOne, playerTwo)
      val result = gameMgr.play(3, 3, 10, realTime = false, randomStart = false, pitchConfig = pitchConfig)
      result match {
        case Some(1) =>
          first += 1
        case Some(2) =>
          second += 1
        case _ =>
          draws += 1
      }
    }

    println(s"Result: $first:$draws:$second")
    if (first > 2) {
      val gameMgr = new GameManager(playerOne, playerTwo)
      gameMgr.play(3, 3, 10, realTime = true, randomStart = false, pitchConfig = pitchConfig)
    }
  }
}
