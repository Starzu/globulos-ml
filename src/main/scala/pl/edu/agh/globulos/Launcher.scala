package pl.edu.agh.globulos

import pl.edu.agh.globulos.game.GameManager
import pl.edu.agh.globulos.ml.{NN3v3VectorPlayer, SmartAtBallPlayer}

import scala.language.postfixOps

object Launcher {
  def main(args: Array[String]): Unit = {
    val teamSize = 3
    val moves = 15
    val playerOne = NN3v3VectorPlayer.load(0.3, "nn3v3_sab_best.model")
//    val playerOne = new AtBallPlayer()
    val playerTwo = new SmartAtBallPlayer()

    // training
//    for (i <- 1 to 200) {
//      playerOne.trainingMode(true)
//      playerTwo.trainingMode(true)
//      val gameMgr = new GameManager(playerOne, playerTwo)
//      println(s"Training $i")
//      val result = gameMgr.play(teamSize, teamSize, moves, realTime = false)
//      println(s"Result: $result")
//    }

    // games
    for (i <- 1 to 5) {
      playerOne.trainingMode(false)
      playerTwo.trainingMode(false)
      val gameMgr = new GameManager(playerOne, playerTwo)
      println(s"Game $i")
      val result = gameMgr.play(teamSize, teamSize, moves, realTime = true)
      println(s"Result: $result")
    }
  }
}
