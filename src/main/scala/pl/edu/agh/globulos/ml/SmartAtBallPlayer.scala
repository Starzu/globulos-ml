package pl.edu.agh.globulos.ml

import org.dyn4j.geometry.Vector2
import pl.edu.agh.globulos.game.Game
import pl.edu.agh.globulos.game.Game.PitchConfig

class SmartAtBallPlayer extends AtBallPlayer {
  override def nextMove(
    ball: Game.Position, ownPlayers: Vector[Game.Position],
    enemyPlayer: Vector[Game.Position], pitchConfig: PitchConfig
  ): Vector[Vector2] = {
    ownPlayers.map { player =>
      if (player.x < ball.x) attackAtBall(ball, pitchConfig, player)
      else {
        new Vector2((ball.x - player.x) * pitchConfig.maxForce, (player.y - ball.y) * pitchConfig.maxForce)
      }
    }
  }
}
