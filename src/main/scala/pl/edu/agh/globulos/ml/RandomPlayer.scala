package pl.edu.agh.globulos.ml

import org.dyn4j.geometry.Vector2
import pl.edu.agh.globulos.game.Game
import pl.edu.agh.globulos.game.Game.PitchConfig

import scala.util.Random

class RandomPlayer extends BasicPlayer {
  override def nextMove(
    ball: Game.Position, ownPlayers: Vector[Game.Position],
    enemyPlayer: Vector[Game.Position], pitchConfig: PitchConfig
  ): Vector[Vector2] = {
    ownPlayers.map { _ =>
      new Vector2((Random.nextDouble() - 0.5) * pitchConfig.maxForce, (Random.nextDouble() - 0.5) * pitchConfig.maxForce)
    }
  }
}
