package pl.edu.agh.globulos.ml

import org.dyn4j.geometry.Vector2
import pl.edu.agh.globulos.game.Game
import pl.edu.agh.globulos.game.Game.PitchConfig

trait Player {
  def beforeGame(): Unit

  /** Player always thinks that he is playing from left to right along X coords. */
  def nextMove(ball: Game.Position, ownPlayers: Vector[Game.Position], enemyPlayer: Vector[Game.Position], pitchConfig: PitchConfig): Vector[Vector2]

  def gameResult(youWon: Boolean, ball: Game.Position, pitchConfig: PitchConfig): Unit

  def trainingMode(enabled: Boolean): Unit

  def save(filename: String): Unit

  protected def deg2Rad(angleDeg: Double): Double = {
    angleDeg / 180 * Math.PI
  }
}

abstract class BasicPlayer extends Player {
  override def beforeGame(): Unit = {}
  override def gameResult(youWon: Boolean, ball: Game.Position, pitchConfig: PitchConfig): Unit = {}
  override def save(filename: String): Unit = {}
  override def trainingMode(enabled: Boolean): Unit = {}
}
