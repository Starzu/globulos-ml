package pl.edu.agh.globulos.ml

import org.dyn4j.geometry.Vector2
import pl.edu.agh.globulos.game.Game
import pl.edu.agh.globulos.game.Game.PitchConfig

class AtBallPlayer extends BasicPlayer {
  override def nextMove(
    ball: Game.Position, ownPlayers: Vector[Game.Position],
    enemyPlayer: Vector[Game.Position], pitchConfig: PitchConfig
  ): Vector[Vector2] = {
    ownPlayers.map { player => attackAtBall(ball, pitchConfig, player) }
  }

  protected def attackAtBall(ball: Game.Position, pitchConfig: PitchConfig, player: Game.Position): Vector2 = {
    new Vector2((ball.x - player.x) * pitchConfig.maxForce, (ball.y - player.y) * pitchConfig.maxForce)
  }
}
