package pl.edu.agh.globulos.ml

import org.deeplearning4j.nn.api.OptimizationAlgorithm
import org.deeplearning4j.nn.conf.layers.{DenseLayer, OutputLayer}
import org.deeplearning4j.nn.conf.{NeuralNetConfiguration, Updater}
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.weights.WeightInit
import org.deeplearning4j.util.ModelSerializer
import org.dyn4j.geometry.Vector2
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.factory.Nd4j
import org.nd4j.linalg.lossfunctions.LossFunctions
import pl.edu.agh.globulos.game.Game
import pl.edu.agh.globulos.game.Game.PitchConfig

import scala.annotation.tailrec
import scala.collection.immutable.NumericRange
import scala.collection.mutable
import scala.util.Random

class NN3v3VectorPlayer private(private var explorationRate: Double, private val network: MultiLayerNetwork) extends Player {
  private var trainingMode: Boolean = false
  private val results: Map[Double, mutable.ArrayBuffer[NN3v3VectorPlayer.Move]] = Map(
    1.0 -> mutable.ArrayBuffer.empty,
    -1.0 -> mutable.ArrayBuffer.empty,
    0.0 -> mutable.ArrayBuffer.empty
  )
  private val moves: mutable.ArrayBuffer[NN3v3VectorPlayer.Move] = mutable.ArrayBuffer.empty

  override def beforeGame(): Unit = {
    moves.clear()
  }

  override def nextMove(ball: Game.Position, ownPlayers: Vector[Game.Position], enemyPlayer: Vector[Game.Position], pitchConfig: PitchConfig): Vector[Vector2] = {
    require(ownPlayers.size == 3, s"Expected 3 own players. Got: ${ownPlayers.size}")
    require(enemyPlayer.size == 3, s"Expected 3 enemy players. Got: ${enemyPlayer.size}")

    val ballNormalised = ball.normalize(pitchConfig)
    val ownNormalised = ownPlayers.map(_.normalize(pitchConfig))
    val enemyNormalised = enemyPlayer.map(_.normalize(pitchConfig))

    val moveVectors = if (trainingMode && Random.nextDouble() < explorationRate) {
      Vector.fill(3)(
        Vector2.create(pitchConfig.maxForce, deg2Rad(Random.nextInt(360)))
      )
    } else {
      @tailrec
      def findAngles(r1: NumericRange[Double], r2: NumericRange[Double], r3: NumericRange[Double]): (Double, Double, Double) = {
        val angles = for {a1 <- r1; a2 <- r2; a3 <- r3} yield (a1, a2, a3)
        val targetAngles = angles.maxBy { case (a1, a2, a3) =>
          val testVector1 = Vector2.create(pitchConfig.maxForce, deg2Rad(a1))
          val testVector2 = Vector2.create(pitchConfig.maxForce, deg2Rad(a2))
          val testVector3 = Vector2.create(pitchConfig.maxForce, deg2Rad(a3))

          testVector1.normalize()
          testVector2.normalize()
          testVector3.normalize()

          network.output(
            Nd4j.create(Array(
              ballNormalised.x, ballNormalised.y,
              ownNormalised(0).x, ownNormalised(0).y, ownNormalised(1).x, ownNormalised(1).y, ownNormalised(2).x, ownNormalised(2).y,
              enemyNormalised(0).x, enemyNormalised(0).y, enemyNormalised(1).x, enemyNormalised(1).y, enemyNormalised(2).x, enemyNormalised(2).y,
              testVector1.x, testVector1.y, testVector2.x, testVector2.y, testVector3.x, testVector3.y
            ))
          ).getDouble(0)
        }

        if (r1.step < 1) targetAngles
        else findAngles(
          Range.Double(targetAngles._1 - r1.step, targetAngles._1 + r1.step, r1.step / 4),
          Range.Double(targetAngles._2 - r2.step, targetAngles._2 + r2.step, r2.step / 4),
          Range.Double(targetAngles._3 - r3.step, targetAngles._3 + r3.step, r3.step / 4)
        )
      }

      val targetAngles = findAngles(Range.Double(0, 360, 45), Range.Double(0, 360, 45), Range.Double(0, 360, 45))
      Vector(
        Vector2.create(pitchConfig.maxForce, deg2Rad(targetAngles._1)),
        Vector2.create(pitchConfig.maxForce, deg2Rad(targetAngles._2)),
        Vector2.create(pitchConfig.maxForce, deg2Rad(targetAngles._3))
      )
    }
    moves += NN3v3VectorPlayer.Move(ballNormalised, ownNormalised, enemyNormalised, moveVectors.map(v => {
      val tmp = v.copy()
      tmp.normalize()
      tmp
    }))
    moveVectors
  }

  override def gameResult(youWon: Boolean, ball: Game.Position, pitchConfig: PitchConfig): Unit = {
    val gameResult: Double = ball match {
      case _ if pitchConfig.isGoal(ball) => 1
      case _ if pitchConfig.isOwnGoal(ball) => -1
      case _ => 0
    }

    val array = results(gameResult)
    array ++= moves
    if (array.size > 100) array.remove(0, array.size - 100)

    results.foreach { case (result, examples) =>
      examples.foreach { move =>
        network.fit(
          Nd4j.create(Array(
            move.ball.x, move.ball.y,
            move.player(0).x, move.player(0).y, move.player(1).x, move.player(1).y, move.player(2).x, move.player(2).y,
            move.opponent(0).x, move.opponent(0).y, move.opponent(1).x, move.opponent(1).y, move.opponent(2).x, move.opponent(2).y,
            move.move(0).x, move.move(0).y, move.move(1).x, move.move(1).y, move.move(2).x, move.move(2).y
          )),
          Nd4j.create(Array(result))
        )
      }
    }
  }

  override def save(filename: String): Unit = {
    ModelSerializer.writeModel(network, filename, true)
  }

  override def trainingMode(enabled: Boolean): Unit = {
    trainingMode = enabled
    if (enabled) explorationRate *= 0.95
  }
}

object NN3v3VectorPlayer {
  private case class Move(ball: Game.Position, player: Vector[Game.Position], opponent: Vector[Game.Position], move: Vector[Vector2])

  def apply(explorationRate: Double): NN3v3VectorPlayer = {
    new NN3v3VectorPlayer(
      explorationRate,
      new MultiLayerNetwork(new NeuralNetConfiguration.Builder()
        .seed(42)
        .iterations(1)
        .learningRate(0.0001)
        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
        .weightInit(WeightInit.XAVIER)
        .updater(Updater.NESTEROVS).momentum(0.05)
        .list()
        // input (ball position, player positions x y, opponent positions, move vectors)
        .layer(0, new DenseLayer.Builder().nIn(20).nOut(100)
          .activation(Activation.SIGMOID)
          .build())
        .layer(1, new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
          .activation(Activation.IDENTITY)
          .nIn(100).nOut(1).build())
        .pretrain(false).backprop(true).build()
      )
    )
  }

  def load(explorationRate: Double, filename: String): NN3v3VectorPlayer = {
    new NN3v3VectorPlayer(explorationRate, ModelSerializer.restoreMultiLayerNetwork(filename, true))
  }
}
