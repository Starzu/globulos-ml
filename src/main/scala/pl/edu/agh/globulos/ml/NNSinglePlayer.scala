package pl.edu.agh.globulos.ml

import org.deeplearning4j.nn.api.OptimizationAlgorithm
import org.deeplearning4j.nn.conf.layers.{DenseLayer, OutputLayer}
import org.deeplearning4j.nn.conf.{NeuralNetConfiguration, Updater}
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.weights.WeightInit
import org.deeplearning4j.util.ModelSerializer
import org.dyn4j.geometry.Vector2
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.factory.Nd4j
import org.nd4j.linalg.lossfunctions.LossFunctions
import pl.edu.agh.globulos.game.Game
import pl.edu.agh.globulos.game.Game.PitchConfig

import scala.collection.mutable
import scala.util.Random

class NNSinglePlayer private(private val network: MultiLayerNetwork) extends Player {
  private var trainingMode: Boolean = false
  private var explorationRate: Double = 1
  private var ballMisses = 1
  private val moves: mutable.ArrayBuffer[NNSinglePlayer.Move] = mutable.ArrayBuffer.empty

  override def beforeGame(): Unit = {
    moves.clear()
  }

  override def nextMove(ball: Game.Position, ownPlayers: Vector[Game.Position], enemyPlayer: Vector[Game.Position], pitchConfig: PitchConfig): Vector[Vector2] = {
    require(ownPlayers.size == 1, s"Expected 1 own players. Got: ${ownPlayers.size}")
    require(enemyPlayer.size == 1, s"Expected 1 enemy players. Got: ${enemyPlayer.size}")

    val ballNormalised = ball.normalize(pitchConfig)
    val playerNormalised = ownPlayers.head.normalize(pitchConfig)

    val v = if (trainingMode && Random.nextDouble() < explorationRate) {
      val a = deg2Rad(Random.nextInt(360))
      Vector2.create(pitchConfig.maxForce, a)
    } else {
      val a = Random.shuffle((0 until 360).toVector).minBy { angle =>
        val testVector = Vector2.create(pitchConfig.maxForce, deg2Rad(angle))
        testVector.normalize()
        val out = network.output(Nd4j.create(Array(ballNormalised.x, ballNormalised.y, playerNormalised.x, playerNormalised.y, testVector.x, testVector.y)))
        out.getDouble(0)
      }
      Vector2.create(pitchConfig.maxForce, deg2Rad(a))
    }
    moves += NNSinglePlayer.Move(ballNormalised, playerNormalised, v)
    Vector(v)
  }

  override def gameResult(youWon: Boolean, ball: Game.Position, pitchConfig: PitchConfig): Unit = {
    val result = {
      ball match {
        case _ if pitchConfig.isGoal(ball) => 0
        case _ if pitchConfig.isOwnGoal(ball) => Math.pow(pitchConfig.pitchWidth, 2) + Math.pow(pitchConfig.pitchHeight, 2)
        case _ => Math.sqrt(Math.pow(pitchConfig.pitchWidth / 2 - ball.x, 2) + Math.pow(ball.y, 2))
      }
    }

    val repeats = {
      if (moves.last.ball != ball) {
        val tmp = ballMisses
        ballMisses = 1
        tmp
      } else {
        ballMisses += 1
        1
      }
    }
    for (_ <- 0 until repeats) {
      moves.foreach { move =>
        val m = move.move
        m.normalize()
        network.fit(
          Nd4j.create(Array(move.ball.x, move.ball.y, move.player.x, move.player.y, m.x, m.y)),
          Nd4j.create(Array(result))
        )
      }
    }
  }

  override def save(filename: String): Unit = {
    ModelSerializer.writeModel(network, filename, true)
  }

  override def trainingMode(enabled: Boolean): Unit = {
    trainingMode = enabled
    if (enabled) explorationRate *= 0.99
  }
}

object NNSinglePlayer {
  private case class Move(ball: Game.Position, player: Game.Position, move: Vector2)

  def apply(): NNSinglePlayer = {
    new NNSinglePlayer(
      new MultiLayerNetwork(new NeuralNetConfiguration.Builder()
        .seed(42)
        .iterations(1)
        .learningRate(0.001)
        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
        .weightInit(WeightInit.XAVIER)
        .updater(Updater.NESTEROVS).momentum(0.3)
        .list()
        .layer(0, new DenseLayer.Builder().nIn(6).nOut(30)
          .activation(Activation.SIGMOID)
          .build())
        .layer(1, new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
          .activation(Activation.IDENTITY)
          .nIn(30).nOut(1).build())
        .pretrain(false).backprop(true).build()
      )
    )
  }

  def load(filename: String): NNSinglePlayer = {
    new NNSinglePlayer(ModelSerializer.restoreMultiLayerNetwork(filename, true))
  }
}