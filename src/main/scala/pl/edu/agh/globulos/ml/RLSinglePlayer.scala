package pl.edu.agh.globulos.ml

import burlap.behavior.singleagent.learning.tdmethods.QLearning
import burlap.mdp.auxiliary.DomainGenerator
import burlap.mdp.core.TerminalFunction
import burlap.mdp.core.action.{Action, ActionType}
import burlap.mdp.core.oo.OODomain
import burlap.mdp.core.oo.propositional.PropositionalFunction
import burlap.mdp.core.oo.state.{OOState, ObjectInstance}
import burlap.mdp.core.state.State
import burlap.mdp.singleagent.environment.SimulatedEnvironment
import burlap.mdp.singleagent.model.statemodel.SampleStateModel
import burlap.mdp.singleagent.model.{FactoredModel, RewardFunction}
import burlap.mdp.singleagent.oo.OOSADomain
import burlap.statehashing.simple.SimpleHashableStateFactory
import org.dyn4j.geometry.Vector2
import pl.edu.agh.globulos
import pl.edu.agh.globulos.game.Game
import pl.edu.agh.globulos.game.Game.PitchConfig
import pl.edu.agh.globulos.ml

import java.util
import scala.beans.BeanProperty
import scala.util.Random

class RLSinglePlayer(filename: Option[String]) extends Player {
  private val pitchConfig = Game.PitchConfig()
  private val domain = new globulos.ml.RLSinglePlayer.SinglePlayerDomain(pitchConfig).generateDomain()
  private val learning = new QLearning(
    domain, 0.5, new SimpleHashableStateFactory(), 0, 0.1, 3
  )
  filename.foreach(learning.loadQTable)


  override def nextMove(ball: Game.Position, ownPlayers: Vector[Game.Position], enemyPlayer: Vector[Game.Position], pitchConfig: Game.PitchConfig): Vector[Vector2] = {
    val state = new ml.RLSinglePlayer.GameState(
      new ml.RLSinglePlayer.BallState(ball.x.toInt, ball.y.toInt),
      new ml.RLSinglePlayer.PlayerState(ownPlayers.head.x.toInt, ownPlayers.head.y.toInt)
    )
    learning.setMaximumEpisodesForPlanning(1)
    val strategy = learning.planFromState(state)
    val move = strategy.action(state).asInstanceOf[RLSinglePlayer.PlayerMove]
    Vector(Vector2.create(pitchConfig.maxForce, deg2Rad(move.angle.toDouble)))
  }

  override def trainingMode(enabled: Boolean): Unit = {
    val initState = new RLSinglePlayer.GameState(
      new ml.RLSinglePlayer.BallState(
        (Random.nextDouble() * pitchConfig.pitchWidth - pitchConfig.pitchWidth / 2).toInt,
        (Random.nextDouble() * pitchConfig.pitchHeight - pitchConfig.pitchHeight / 2).toInt
      ),
      new ml.RLSinglePlayer.PlayerState(
        (Random.nextDouble() * pitchConfig.pitchWidth - pitchConfig.pitchWidth / 2).toInt,
        (Random.nextDouble() * pitchConfig.pitchHeight - pitchConfig.pitchHeight / 2).toInt
      )
    )
    val env = new SimulatedEnvironment(domain, initState)
    learning.runLearningEpisode(env, 1)
  }

  override def save(filename: String): Unit = {
    learning.writeQTable(filename)
  }

  override def beforeGame(): Unit = {}
  override def gameResult(youWon: Boolean, ball: Game.Position, pitchConfig: Game.PitchConfig): Unit = {}
}

object RLSinglePlayer {
  private val PositionX = "X"
  private val PositionY = "Y"

  private val GameStateName = "game"
  private val BallStateName = "ball"
  private val PlayerStateName = "player"

  private val PlayerMoveActionType = "player_move_type"
  private val PlayerMoveAction = "player_move"

  private val GoalPFName = "goal"
  private val OwnGoalPFName = "own_goal"

  private abstract class GameObjectState(@BeanProperty protected val x: java.lang.Integer = 0,
    @BeanProperty protected val y: java.lang.Integer = 0) extends ObjectInstance {
    override def copy(): GameObjectState

    override def variableKeys(): util.List[AnyRef] =
      util.Arrays.asList(PositionX, PositionY)

    override def get(variableKey: scala.Any): AnyRef =
      variableKey match {
        case PositionX => new java.lang.Integer(x)
        case PositionY => new java.lang.Integer(y)
      }

    override def copyWithName(objectName: String): ObjectInstance = {
      if (!objectName.equals(BallStateName))
        throw new RuntimeException("Ball name must be " + name())
      copy()
    }

    override def className(): String =
      name()
  }

  private class BallState(_x: Int = 0, _y: Int = 0) extends GameObjectState(_x, _y) {
    override def name(): String = BallStateName

    override def copy(): BallState =
      new BallState(x, y)
  }

  private class PlayerState(_x: Int = 0, _y: Int = 0) extends GameObjectState(_x, _y) {
    override def name(): String = PlayerStateName

    override def copy(): PlayerState =
      new PlayerState(x, y)
  }

  private class GameState(
    @BeanProperty protected val ball: BallState,
    @BeanProperty protected val player: PlayerState
  ) extends OOState {
    override def variableKeys(): util.List[AnyRef] =
      util.Arrays.asList(BallStateName, PlayerStateName)

    override def get(variableKey: scala.Any): AnyRef =
      variableKey match {
        case BallStateName => ball
        case PlayerStateName => player
      }

    override def copy(): State =
      new GameState(ball.copy(), player.copy())

    override def objectsOfClass(oclass: String): util.List[ObjectInstance] = {
      oclass match {
        case BallStateName => util.Arrays.asList(ball)
        case PlayerStateName => util.Arrays.asList(player)
      }
    }

    override def objects(): util.List[ObjectInstance] = {
      util.Arrays.asList(ball, player)
    }

    override def numObjects(): Int = 2

    override def `object`(oname: String): ObjectInstance = {
      oname match {
        case BallStateName => ball
        case PlayerStateName => player
      }
    }
  }

  private class PlayerMove(@BeanProperty val angle: java.lang.Integer) extends Action {
    override def actionName(): String =
      PlayerMoveAction + angle.toString

    override def copy(): PlayerMove =
      new PlayerMove(angle)
  }

  private class PlayerMoveType extends ActionType {
    private val possibleActions: Map[String, Action] =
      (0 until 360).map { angle =>
        val action = new PlayerMove(angle)
        (action.actionName(), action)
      }.toMap

    override def typeName(): String =
      PlayerMoveActionType

    override def associatedAction(strRep: String): Action =
      possibleActions(strRep)

    override def allApplicableActions(s: State): util.List[Action] =
      util.Arrays.asList(possibleActions.values.toList: _*)
  }

  private class GoalPF(pitchConfig: PitchConfig) extends PropositionalFunction(GoalPFName, Array(BallStateName)) {
    override def isTrue(s: OOState, params: String*): Boolean = {
      val ball = s.`object`(params(0)).asInstanceOf[BallState]
      pitchConfig.isGoal(Game.Position(ball.get(PositionX).asInstanceOf[java.lang.Integer].toDouble, ball.get(PositionY).asInstanceOf[java.lang.Integer].toDouble))
    }
  }

  private class OwnGoalPF(pitchConfig: PitchConfig) extends PropositionalFunction(OwnGoalPFName, Array(BallStateName)) {
    override def isTrue(s: OOState, params: String*): Boolean = {
      val ball = s.`object`(params(0)).asInstanceOf[BallState]
      pitchConfig.isOwnGoal(Game.Position(ball.get(PositionX).asInstanceOf[java.lang.Integer].toDouble, ball.get(PositionY).asInstanceOf[java.lang.Integer].toDouble))
    }
  }

  private class SinglePlayerReward(domain: OODomain) extends RewardFunction {
    override def reward(s: State, a: Action, sprime: State): Double = {
      val goalPF = domain.propFunction(GoalPFName).asInstanceOf[GoalPF]
      val ownGoalPF = domain.propFunction(OwnGoalPFName).asInstanceOf[OwnGoalPF]

      if (goalPF.someGroundingIsTrue(sprime.asInstanceOf[OOState])) 100
      else if (ownGoalPF.someGroundingIsTrue(sprime.asInstanceOf[OOState])) -100
      else -1
    }
  }

  private class SinglePlayerTerminalFunction(domain: OODomain) extends TerminalFunction {
    override def isTerminal(s: State): Boolean = {
      domain.propFunction(GoalPFName).asInstanceOf[GoalPF]
        .someGroundingIsTrue(s.asInstanceOf[OOState])
    }
  }

  private class SinglePlayerModel(pitchConfig: PitchConfig) extends SampleStateModel {
    override def sample(s: State, a: Action): State = {
      val game = new Game(0, 1, false, pitchConfig)

      val ball = s.get(BallStateName).asInstanceOf[BallState]
      val player = s.get(PlayerStateName).asInstanceOf[PlayerState]
      game.forcePositions(
        Game.Position(ball.get(PositionX).asInstanceOf[java.lang.Integer].toDouble, ball.get(PositionY).asInstanceOf[java.lang.Integer].toDouble),
        Seq.empty,
        Seq(Game.Position(player.get(PositionX).asInstanceOf[java.lang.Integer].toDouble, player.get(PositionY).asInstanceOf[java.lang.Integer].toDouble))
      )

      val move = Vector2.create(pitchConfig.maxForce, a.asInstanceOf[PlayerMove].angle.toDouble / 180 * Math.PI)
      game.continueToNextStaticPoint(Vector.empty, Vector(move))

      val ballPosition = game.ballPosition
      val playerPosition = game.playerTwoPositions.head
      new GameState(
        new BallState(ballPosition.x.toInt, ballPosition.y.toInt),
        new PlayerState(playerPosition.x.toInt, playerPosition.y.toInt)
      )
    }
  }

  private class SinglePlayerDomain(pitchConfig: PitchConfig) extends DomainGenerator {
    override def generateDomain(): OOSADomain = {
      val domain = new OOSADomain

      domain
        .addStateClass(BallStateName, classOf[BallState])
        .addStateClass(PlayerStateName, classOf[PlayerState])

      domain
        .addActionType(new PlayerMoveType)

      OODomain.Helper.addPfsToDomain(domain, util.Arrays.asList(
        new GoalPF(pitchConfig), new OwnGoalPF(pitchConfig)
      ))

      val spModel = new SinglePlayerModel(pitchConfig)
      val rewardFunction = new SinglePlayerReward(domain)
      val terminateFunction = new SinglePlayerTerminalFunction(domain)
      val model = new FactoredModel(spModel, rewardFunction, terminateFunction)

      domain.setModel(model)

      domain
    }
  }
}