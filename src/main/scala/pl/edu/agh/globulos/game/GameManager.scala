package pl.edu.agh.globulos.game

import org.dyn4j.geometry.Vector2
import pl.edu.agh.globulos.game.Game.PitchConfig
import pl.edu.agh.globulos.ml.Player

import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.Random

class GameManager(playerOne: Player, playerTwo: Player) {
  def play(
    teamOneSize: Int, teamTwoSize: Int, movesLimit: Int = Int.MaxValue,
    realTime: Boolean = true, randomStart: Boolean = false, pitchConfig: PitchConfig = PitchConfig()
  ): Option[Int] = {
    val game = new Game(teamOneSize, teamTwoSize, realTime, pitchConfig)

    if (randomStart) {
      def randomPosition(pitchConfig: PitchConfig): Game.Position = {
        Game.Position(
          0.9 * Random.nextDouble() * pitchConfig.pitchWidth - pitchConfig.pitchWidth / 2,
          0.9 * Random.nextDouble() * pitchConfig.pitchHeight - pitchConfig.pitchHeight / 2
        )
      }

      val positions = mutable.ArrayBuffer.empty[Game.Position]
      while (positions.size < 1 + teamOneSize + teamTwoSize) {
        val p = randomPosition(game.pitchConfig)
        if (!positions.exists((p2: Game.Position) => Math.pow(p.x - p2.x, 2) + Math.pow(p.y - p2.y, 2) < 1.7))
          positions += p
      }

      game.forcePositions(
        ballPosition = positions.head,
        playerOnePositions = positions.slice(1, teamOneSize + 1),
        playerTwoPositions = positions.drop(teamOneSize + 1)
      )
    }

    @tailrec
    def nextMove(moves: Int): Option[Int] = {
      if (moves > movesLimit) {
        playerOne.gameResult(youWon = false, game.ballPosition, game.pitchConfig)
        playerTwo.gameResult(youWon = false, mapForPlayerTwo(game.ballPosition), game.pitchConfig)
        None
      } else {
        val movesOne = playerOne.nextMove(
          game.ballPosition,
          game.playerOnePositions,
          game.playerTwoPositions,
          game.pitchConfig
        )
        val movesTwo = playerTwo.nextMove(
          mapForPlayerTwo(game.ballPosition),
          game.playerTwoPositions.map(mapForPlayerTwo),
          game.playerOnePositions.map(mapForPlayerTwo),
          game.pitchConfig
        ).map(mapForPlayerTwo)

        val result = game.continueToNextStaticPoint(movesOne, movesTwo)

        result match {
          case Some(winner) =>
            playerOne.gameResult(winner == 1, game.ballPosition, game.pitchConfig)
            playerTwo.gameResult(winner == 2, mapForPlayerTwo(game.ballPosition), game.pitchConfig)
            Some(winner)
          case None =>
            nextMove(moves + 1)
        }
      }
    }

    playerOne.beforeGame()
    playerTwo.beforeGame()
    val result = nextMove(1)
    game.hide()
    result
  }

  private def mapForPlayerTwo(position: Game.Position): Game.Position =
    Game.Position(-position.x, position.y)

  private def mapForPlayerTwo(move: Vector2): Vector2 =
    new Vector2(-move.x, move.y)
}
