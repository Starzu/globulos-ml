package pl.edu.agh.globulos.game

import org.dyn4j.dynamics.{Body, World}
import org.dyn4j.geometry.{Circle, MassType, Rectangle, Vector2}
import pl.edu.agh.globulos.game.Game.PitchConfig

import java.awt._
import java.awt.geom.AffineTransform
import javax.swing.{JFrame, UIManager}
import scala.collection.JavaConverters._

class Game(
  val teamOneSize: Int, val teamTwoSize: Int, val realTime: Boolean,
  val pitchConfig: PitchConfig = PitchConfig(), val initialX: Double = 7.0
) {

  private val targetSize = new Dimension(1200, 800)

  private val ball: Game.Ball = new Game.Ball()
  private val teamOne: Vector[Game.Player] = Vector.fill(teamOneSize)(new Game.Player(Color.BLUE))
  private val teamTwo: Vector[Game.Player] = Vector.fill(teamTwoSize)(new Game.Player(Color.RED))

  teamOne.zipWithIndex.foreach { case (player, idx) =>
    player.translate(-initialX, 3 * (idx - teamOneSize / 2))
  }
  teamTwo.zipWithIndex.foreach { case (player, idx) =>
    player.translate(initialX, 3 * (idx - teamTwoSize / 2))
  }

  private val world: World = {
    val w = new World()
    w.setGravity(World.ZERO_GRAVITY)
    val settings = w.getSettings
    settings.setVelocityConstraintSolverIterations(1000)
    w.setSettings(settings)

    w.addBody(ball)
    teamOne.foreach(w.addBody)
    teamTwo.foreach(w.addBody)

    // add walls
    val topWall = new Game.Wall(pitchConfig.pitchWidth, pitchConfig.wallWidth)
    val bottomWall = new Game.Wall(pitchConfig.pitchWidth, pitchConfig.wallWidth)
    val leftTopWall = new Game.Wall(pitchConfig.wallWidth, (pitchConfig.pitchHeight - pitchConfig.goalWidth + pitchConfig.wallWidth) / 2)
    val leftBottomWall = new Game.Wall(pitchConfig.wallWidth, (pitchConfig.pitchHeight - pitchConfig.goalWidth + pitchConfig.wallWidth) / 2)
    val rightTopWall = new Game.Wall(pitchConfig.wallWidth, (pitchConfig.pitchHeight - pitchConfig.goalWidth + pitchConfig.wallWidth) / 2)
    val rightBottomWall = new Game.Wall(pitchConfig.wallWidth, (pitchConfig.pitchHeight - pitchConfig.goalWidth + pitchConfig.wallWidth) / 2)
    val leftGoalBack = new Game.Wall(pitchConfig.wallWidth, pitchConfig.goalWidth)
    val leftGoalTop = new Game.Wall(pitchConfig.goalDepth - pitchConfig.wallWidth / 2, pitchConfig.wallWidth)
    val leftGoalBottom = new Game.Wall(pitchConfig.goalDepth - pitchConfig.wallWidth / 2, pitchConfig.wallWidth)
    val rightGoalBack = new Game.Wall(pitchConfig.wallWidth, pitchConfig.goalWidth)
    val rightGoalTop = new Game.Wall(pitchConfig.goalDepth - pitchConfig.wallWidth / 2, pitchConfig.wallWidth)
    val rightGoalBottom = new Game.Wall(pitchConfig.goalDepth - pitchConfig.wallWidth / 2, pitchConfig.wallWidth)

    topWall.translate(0, pitchConfig.pitchHeight / 2)
    bottomWall.translate(0, -pitchConfig.pitchHeight / 2)
    leftTopWall.translate(-(pitchConfig.pitchWidth + pitchConfig.wallWidth) / 2, (pitchConfig.pitchHeight + pitchConfig.goalWidth + pitchConfig.wallWidth) / 4)
    leftBottomWall.translate(-(pitchConfig.pitchWidth + pitchConfig.wallWidth) / 2, -((pitchConfig.pitchHeight + pitchConfig.goalWidth + pitchConfig.wallWidth) / 4))
    rightTopWall.translate((pitchConfig.pitchWidth + pitchConfig.wallWidth) / 2, (pitchConfig.pitchHeight + pitchConfig.goalWidth + pitchConfig.wallWidth) / 4)
    rightBottomWall.translate((pitchConfig.pitchWidth + pitchConfig.wallWidth) / 2, -((pitchConfig.pitchHeight + pitchConfig.goalWidth + pitchConfig.wallWidth) / 4))
    leftGoalBack.translate(-(pitchConfig.goalDepth + pitchConfig.pitchWidth / 2), 0)
    leftGoalTop.translate(-(pitchConfig.goalDepth + pitchConfig.pitchWidth) / 2 - pitchConfig.wallWidth * 0.75, (pitchConfig.goalWidth + pitchConfig.wallWidth) / 2)
    leftGoalBottom.translate(-(pitchConfig.goalDepth + pitchConfig.pitchWidth) / 2 - pitchConfig.wallWidth * 0.75, -(pitchConfig.goalWidth + pitchConfig.wallWidth) / 2)
    rightGoalBack.translate(pitchConfig.goalDepth + pitchConfig.pitchWidth / 2, 0)
    rightGoalTop.translate((pitchConfig.goalDepth + pitchConfig.pitchWidth) / 2 + pitchConfig.wallWidth * 0.75, (pitchConfig.goalWidth + pitchConfig.wallWidth) / 2)
    rightGoalBottom.translate((pitchConfig.goalDepth + pitchConfig.pitchWidth) / 2 + pitchConfig.wallWidth * 0.75, -(pitchConfig.goalWidth + pitchConfig.wallWidth) / 2)

    w.addBody(topWall)
    w.addBody(bottomWall)
    w.addBody(leftTopWall)
    w.addBody(leftBottomWall)
    w.addBody(rightTopWall)
    w.addBody(rightBottomWall)
    w.addBody(leftGoalBack)
    w.addBody(leftGoalTop)
    w.addBody(leftGoalBottom)
    w.addBody(rightGoalBack)
    w.addBody(rightGoalTop)
    w.addBody(rightGoalBottom)

    w.step(1)
    w
  }


  private lazy val frame = new JFrame("globs-ml")
  private lazy val canvas: Canvas = if (realTime) {
    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName)

    val c = new Canvas
    c.setPreferredSize(targetSize)
    c.setMinimumSize(targetSize)
    c.setMaximumSize(targetSize)
    c.setIgnoreRepaint(true)
    frame.add(c) // add canvas to frame
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    frame.setResizable(false)
    frame.setVisible(true)
    frame.pack()
    c.createBufferStrategy(2)
    c
  } else null

  def hide(): Unit = {
    if (realTime) {
      frame.setVisible(false)
      frame.dispose()
    }
  }

  def ballPosition: Game.Position = {
    val p = ball.getTransform
    Game.Position(p.getTranslationX, p.getTranslationY)
  }

  def playerOnePositions: Vector[Game.Position] =
    playerPositions(teamOne)

  def playerTwoPositions: Vector[Game.Position] =
    playerPositions(teamTwo)

  def forcePositions(ballPosition: Game.Position, playerOnePositions: Seq[Game.Position], playerTwoPositions: Seq[Game.Position]): Unit = {
    ball.translateToOrigin()
    ball.translate(ballPosition.x, ballPosition.y)
    teamOne.zip(playerOnePositions).foreach { case (player, position) =>
      player.translateToOrigin()
      player.translate(position.x, position.y)
    }
    teamTwo.zip(playerTwoPositions).foreach { case (player, position) =>
      player.translateToOrigin()
      player.translate(position.x, position.y)
    }
  }

  private def playerPositions(team: Vector[Game.Player]): Vector[Game.Position] =
    team.map { player =>
      val p = player.getTransform
      Game.Position(p.getTranslationX, p.getTranslationY)
    }

  def continueToNextStaticPoint(teamOneMoves: Vector[Vector2], teamTwoMoves: Vector[Vector2]): Option[Int] = {
    val moves = teamOne.zip(teamOneMoves) ++ teamTwo.zip(teamTwoMoves)
    moves.foreach { case (player, move) =>
      if (move.getMagnitude > pitchConfig.maxForce) {
        move.multiply(pitchConfig.maxForce / move.getMagnitude)
        assert(move.getMagnitude - pitchConfig.maxForce < 1e-6, s"${move.getMagnitude} > ${pitchConfig.maxForce}")
      }
      player.applyForce(move)
    }

    var lastUpdate = System.nanoTime()
    var static = false
    var winner: Option[Int] = None
    while (winner.isEmpty && !static) {
      if (realTime) {
        Thread.sleep(10)
        val time = System.nanoTime()
        val diff = time - lastUpdate
        lastUpdate = time
        world.update(diff)
      } else {
        world.step(5)
      }

      val p = ballPosition
      if (p.x < -pitchConfig.pitchWidth / 2) winner = Some(2)
      if (p.x > pitchConfig.pitchWidth / 2) winner = Some(1)

      import scala.collection.JavaConverters._
      static = world.getBodies.asScala.forall(body => body.getChangeInPosition.getMagnitude < 5e-3) // TODO better equals?

      if (realTime) {
        val g = canvas.getBufferStrategy.getDrawGraphics.asInstanceOf[Graphics2D]

        // flip the y axis and move the origin to the center
        val yFlip = AffineTransform.getScaleInstance(1, -1)
        val move = AffineTransform.getTranslateInstance(targetSize.width / 2, -targetSize.height / 2)
        g.transform(yFlip)
        g.transform(move)

        render(g)
        g.dispose()

        val strategy = canvas.getBufferStrategy
        if (!strategy.contentsLost) strategy.show()

        Toolkit.getDefaultToolkit.sync()
      }
    }
    winner
  }

  protected def render(g: Graphics2D): Unit = if (realTime) {
    g.setColor(Color.WHITE)
    g.fillRect(-targetSize.width / 2, -targetSize.height / 2, targetSize.width, targetSize.height)
    g.translate(0.0, -1.0 * Game.GraphicsScale)
    for (body <- world.getBodies.asScala) body match {
      case body: Game.GameObject => body.render(g)
      case _ => //ignore
    }
  }
}

object Game {
  val GraphicsScale = 20 // px per meter

  case class Position(x: Double, y: Double) {
    def normalize(pitchConfig: PitchConfig): Position =
      Position(x / pitchConfig.pitchWidth + 0.5, y / pitchConfig.pitchHeight + 0.5)
  }

  case class PitchConfig(pitchWidth: Double = 34.0, pitchHeight: Double = 25.0, goalWidth: Double = 7.0,
                         goalDepth: Double = 2.5, wallWidth: Double = 0.2, maxForce: Double = 1400.0) {
    def isGoal(ball: Game.Position): Boolean =
      ball.x >= pitchWidth / 2

    def isOwnGoal(ball: Game.Position): Boolean =
      ball.x <= -pitchWidth / 2
  }

  private class GameObject(val color: Color) extends Body {
    def render(g: Graphics2D): Unit = {
      val ot = g.getTransform

      val lt = new AffineTransform
      lt.translate(transform.getTranslationX * GraphicsScale, transform.getTranslationY * GraphicsScale)
      lt.rotate(transform.getRotation)
      g.transform(lt)

      fixtures.asScala.foreach { fixture =>
        val convex = fixture.getShape
        Graphics2DRenderer.render(g, convex, GraphicsScale, color)
      }

      g.setTransform(ot)
    }
  }

  private class Player(color: Color) extends GameObject(color){
    addFixture(new Circle(0.8), 1, 0, 0.8)
    setAngularDamping(1)
    setLinearDamping(1)
    setMass(MassType.NORMAL)
  }

  private class Ball extends GameObject(Color.DARK_GRAY) {
    addFixture(new Circle(0.3), 0.1, 0, 0.8)
    setAngularDamping(1)
    setLinearDamping(1)
    setMass(MassType.NORMAL)
  }

  private class Wall(width: Double, height: Double) extends GameObject(Color.LIGHT_GRAY) {
    addFixture(new Rectangle(width, height), 100000, 0, 0.8)
    setMass(MassType.INFINITE)
  }
}