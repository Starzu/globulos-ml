package pl.edu.agh.globulos

import pl.edu.agh.globulos.game.GameManager
import pl.edu.agh.globulos.ml.{NoopPlayer, Player, RLSinglePlayer}

import scala.language.postfixOps

object RLSinglePlayerTraining {
  def main(args: Array[String]): Unit = {
    val iterations = args(0).toInt
    val batch = args(1).toInt
    val testIterations = args(2).toInt
    val filenamePrefix = args(3)
    val loadFromFile = if (args.length >= 5) Some(args(4)) else None

    val playerOne = new NoopPlayer
    val playerTwo = new RLSinglePlayer(loadFromFile)

    for (i <- 1 to (iterations / batch)) {
      println(s"Batch: $i")
      for (x <- 1 to batch) {
        playerTwo.trainingMode(true)
      }
      test(testIterations, playerOne, playerTwo)
//      playerTwo.save(s"${filenamePrefix}_${i * batch}.model")
    }
  }

  def test(iterations: Int, playerOne: Player, playerTwo: Player): Unit = {
    val goals = (1 to iterations).map { i =>
      val gameMgr = new GameManager(playerOne, playerTwo)
      val result = gameMgr.play(0, 1, 3, realTime = false, randomStart = true)
      if (result.contains(2)) 1 else 0
    }.sum
    println(s"Result: ${goals.toDouble / iterations}")
  }
}
