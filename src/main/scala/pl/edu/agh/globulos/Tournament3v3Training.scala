package pl.edu.agh.globulos

import pl.edu.agh.globulos.game.Game.PitchConfig
import pl.edu.agh.globulos.game.GameManager
import pl.edu.agh.globulos.ml._

import scala.language.postfixOps

object Tournament3v3Training {
  val pitchConfig: PitchConfig = PitchConfig(
    pitchWidth = 20, pitchHeight = 13, maxForce = 1200
  )

  val teams: Seq[(String, () => Player)] = Tournament3v3.teams

  def main(args: Array[String]): Unit = {
    val bigNN = NN3v3VectorPlayer(1)
    val p1 = ("BigNN", () => bigNN)
    val testEvery = 20
    for (batch <- 1 to 1000) {
      bigNN.trainingMode(batch % testEvery != 0)

      if (batch % testEvery == 0) bigNN.save(s"big_nn_$batch.model")

      for (i <- teams.indices) {
        val p2 = teams(i)

        if (batch % testEvery == 0) println(s"$batch. ${p1._1} vs ${p2._1}")

        var (first, draws, second) = (0, 0, 0)
        (1 to 5).foreach { _ =>
          val gameMgr = new GameManager(bigNN, p2._2())
          gameMgr.play(3, 3, 5, realTime = false, randomStart = false, pitchConfig = pitchConfig) match {
            case Some(1) =>
              first += 1
            case Some(2) =>
              second += 1
            case _ =>
              draws += 1
          }
        }

        if (batch % testEvery == 0) println(s"Result: $first:$draws:$second")
      }
    }
  }
}
