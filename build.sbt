name := "globulos-ml"
version := "1.0"
scalaVersion := "2.12.1"

assembly / mainClass := Some("pl.edu.agh.globulos.NNSinglePlayerTraining")

libraryDependencies += "org.dyn4j" % "dyn4j" % "3.2.3"
libraryDependencies += "org.deeplearning4j" % "deeplearning4j-core" % "0.8.0"
libraryDependencies += "org.nd4j" % "nd4j-native-platform" % "0.8.0"
libraryDependencies += "edu.brown.cs.burlap" % "burlap" % "3.0.1"